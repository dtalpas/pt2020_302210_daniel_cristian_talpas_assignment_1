package PT2020;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import static java.lang.System.*;

public class Main {
    static Polynomial polinom1 = new Polynomial();
    static Polynomial polinom2 = new Polynomial();


    public static List<String> splitInput(String exp){
        List<String> split = new ArrayList<String>();

        for(int i = 0; i < exp.length(); i++) {
            int j = i;
            while (j < exp. length() && (exp.charAt(j) != '+' && exp.charAt(j) != '-')) {
                j++;
            }
            if (i > 0 && (exp.charAt(i - 1) == '-' || exp.charAt(i - 1) == '+')) {
                split.add(exp.substring(i - 1, j));
            } else {
                split.add(exp.substring(i, j));
            }
            i = j;
        }
        if(exp.charAt(0) == '-' || exp.charAt(0) == '+'){
            split.remove(0);
        }

//        for (String s : split) {
//            out.println(s);
//        }
        return split;
    }

    public static Polynomial prelucreazaInput(List<String> split){
        Polynomial polynomialToReturn = new Polynomial();

        for (String s : split) {
            String coeffS = "0";
            String powerS = "0";
            int coeff = 0;
            int power = 0;

            //POWER
            if(!s.contains("^{")){
                if(s.indexOf('X') == -1){
                    powerS = "0";
                }
                else{
                    powerS = "1";
                }
            }else{
                powerS = s.substring(s.indexOf("^{")+2, s.indexOf('}'));
            }

            //COEFFICIENT
            if(s.indexOf('X') == -1){
                coeffS = s;
            }
            else {
                if(s.charAt(0) == '-' && s.charAt(1) == 'X'){
                    coeffS = "-1";
                }
                else if(s.charAt(0) == '+' && s.charAt(1) == 'X'){
                    coeffS = "1";
                }
                else {
                    coeffS = s.substring(0, s.indexOf('X'));
                }
            }

            coeff = Integer.parseInt(coeffS);
            power = Integer.parseInt(powerS);
            polynomialToReturn.addMonom(new Monomial(coeff, power));
        }
        return polynomialToReturn;
    }

    public static Polynomial getProperInput(String inputString){
        return (prelucreazaInput(splitInput(inputString)));
    }

    public static void popUp(String mesaj) {

        JOptionPane.showMessageDialog(null,
                mesaj,
                "Eroare",
                JOptionPane.ERROR_MESSAGE);
    }

    public static boolean isWrongInput(String giventString){
        if(giventString.isEmpty()){
            return true;
        }

        String LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWYZ";
        String letters = "abcdefghijklmnopqrstuvwxyz";
        String symbols = "`~!@#$%&*()_=|\\][';:/\",.<>\n ";

        for(int i = 0; i < LETTERS.length()-1; i++){
            if(giventString.contains(LETTERS.substring(i, i+1))){
                return true;
            }
        }
        for(int i = 0; i < letters.length()-1; i++){
            if(giventString.contains(letters.substring(i, i+1))){
                return true;
            }
        }
        for(int i = 0; i < symbols.length()-1; i++){
            if(giventString.contains(symbols.substring(i, i+1))){
                return true;
            }
        }

        if(  (giventString.contains("^{") && !giventString.contains("}")) || (giventString.contains("^") && !giventString.contains("{"))  ) {
            return true;
        }
        return false;
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Polynomial Calculator");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(700, 350);
        frame.setLocation(300, 300);

        //------- TEXT FIELDS
        final JTextField P = new JTextField(25);      // polinom P
        final JTextField Q = new JTextField(25);      // polinom QString rezultat = "";
        final JTextField R = new JTextField(25);      // polinom R

        //------- LABELS
        JLabel labelP = new JLabel("Polinom P: ");
        JLabel labelQ = new JLabel("Polinom Q: ");
        JLabel labelR = new JLabel("Rezultat: ");
        JLabel labelNotification = new JLabel("The data input MUST match the following syntax:  aX^{n} ± bX^{n-1} ± ... ± ζX ± γ");

        //------- MENIU OPTIUNI
        final JButton adunare   = new JButton("Adunare");
        JButton scadere   = new JButton("Scadere");
        JButton inmultire = new JButton("Inmultire");
        JButton impartire = new JButton("Impartire");
        JButton derivare  = new JButton("Derivare");
        JButton integrare = new JButton("Integrare");
        JButton clearText = new JButton("Clear all");

        //------- PANELS
        JPanel panel1 = new JPanel();
        JPanel panel2 = new JPanel();
        JPanel panelMeniu = new JPanel();
        JPanel panelMeniu1 = new JPanel();
        JPanel panelMeniu2 = new JPanel();
        JPanel panelInput = new JPanel();
        JPanel panelOutput = new JPanel();
        JPanel mainPanel = new JPanel();


        panel1.setLayout(new BoxLayout(panel1, BoxLayout.Y_AXIS));
        panel2.setLayout(new BoxLayout(panel2, BoxLayout.Y_AXIS));
        panelMeniu.setLayout(new BoxLayout(panelMeniu, BoxLayout.Y_AXIS));
        panelInput.add(panel1);
        panelInput.add(Box.createRigidArea(new Dimension(25, 0)));
        panelInput.add(panel2);
        panel1.add(labelP);
        panel1.add(labelQ);
        panel2.add(P);
        panel2.add(Q);
        panelInput.add(panel1);
        panelInput.add(panel2);
        panelInput.add(labelNotification);
        panelOutput.add(labelR);
        panelOutput.add(R);
        panelOutput.add(clearText);

        panelMeniu1.add(adunare);
        panelMeniu1.add(scadere);
        panelMeniu1.add(inmultire);
        panelMeniu2.add(impartire);
        panelMeniu2.add(derivare);
        panelMeniu2.add(integrare);

        panelMeniu.add(panelMeniu1);
        panelMeniu.add(panelMeniu2);

        //------- BUTOANE
        adunare.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String inputP = P.getText();
                String inputQ = Q.getText();
                if (!isWrongInput(inputP) && !isWrongInput(inputQ)) {
                    polinom1 = getProperInput(inputP);
                    polinom2 = getProperInput(inputQ);

                    String rez = Operations.add(polinom1, polinom2).toString();
                    try {
                        R.setText(rez);
                    } catch (Exception Err) {
                        popUp("Eroare neprevazuta <=> rezultat neasteptat");
                    }
                    polinom1.getPolinom().clear();
                    polinom2.getPolinom().clear();
                }
                else{
                    popUp("Eroare de Sintaxa");
                }
            }
        });
        scadere.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String inputP = P.getText();
                String inputQ = Q.getText();

                if (!isWrongInput(inputP) && !isWrongInput(inputQ)) {
                    polinom1 = getProperInput(inputP);
                    polinom2 = getProperInput(inputQ);

                    String rez = Operations.sub(polinom1, polinom2).toString();
                    try {
                        R.setText(rez);
                    } catch (Exception Err) {
                        popUp("Eroare neprevazuta <=> rezultat neasteptat");
                    }
                    polinom1.getPolinom().clear();
                    polinom2.getPolinom().clear();
                }
                else{
                    popUp("Eroare de Sintaxa");
                }
            }
        });
        inmultire.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String inputP = P.getText();
                String inputQ = Q.getText();

                if (!isWrongInput(inputP) && !isWrongInput(inputQ)) {
                    polinom1 = getProperInput(inputP);
                    polinom2 = getProperInput(inputQ);

                    String rez = Operations.mul(polinom1, polinom2).toString();
                    try {
                        R.setText(rez);
                    } catch (Exception Err) {
                        popUp("Eroare neprevazuta <=> rezultat neasteptat");
                    }
                    polinom1.getPolinom().clear();
                    polinom2.getPolinom().clear();
                }
                else{
                    popUp("Eroare de sintaxa!");
                }
            }
        });
        impartire.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                R.setText("Actiune indisponibila :(");
            }
        });
        derivare.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String inputP = P.getText();
                if(!isWrongInput(inputP)) {
                    polinom1 = getProperInput(inputP);
                    String rez = Operations.deriv(polinom1).toString();
                    try {
                        R.setText(rez);
                    } catch (Exception Err) {
                        popUp("Eroare neprevazuta <=> rezultat neasteptat");
                    }
                }else{
                    popUp("Eroare de sintaxa!");
                }
                polinom1.getPolinom().clear();
            }
        });
        integrare.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String inputP = P.getText();
                if (!isWrongInput(inputP)) {
                    polinom1 = getProperInput(inputP);
                    String rez = Operations.integrate(polinom1).toString();
                    try {
                        R.setText(rez);
                    } catch (Exception Err) {
                        popUp("Eroare neprevazuta <=> rezultat neasteptat");
                    }
                    polinom1.getPolinom().clear();
                } else {
                    popUp("Eroare de Sintaxa");
                }
            }
        });

        clearText.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                polinom1.getPolinom().clear();
                polinom2.getPolinom().clear();
                P.setText("");
                Q.setText("");
                R.setText("");
            }
        });

        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
        mainPanel.add(panelInput);
        mainPanel.add(panelMeniu);
        mainPanel.add(panelOutput);

        frame.setContentPane(mainPanel);
        frame.setVisible(true);
    }
}
