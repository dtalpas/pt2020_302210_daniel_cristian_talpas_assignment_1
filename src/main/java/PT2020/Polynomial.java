package PT2020;

import java.util.*;
import static java.lang.Math.*;

public class Polynomial {
    private List<Monomial> polinom = new ArrayList<Monomial>();

    public void addMonom(Monomial monom){
        polinom.add(monom);
        this.orderByPower();
    }

    public List<Monomial> getPolinom() {
        return polinom;
    }

    public void setPolinom(List<Monomial> polinom) {
        this.polinom = polinom;
    }

    public void orderByPower(){
        Collections.sort(this.polinom, Collections.<Monomial>reverseOrder());
    }

    public int getDegree(){
        if(polinom.size() == 0){return -1;}
        return polinom.get(0).getPower();
    }

    public boolean isEmpty(){
        return polinom.isEmpty();  //return true if 'polinome' is empty
    }

    public int getMonoameNo(){
        return this.polinom.size();
    }

    public Monomial getMonomByIndex(int index){
        return this.polinom.get(index);
    }

    @Override
    public String toString() {
        if(this.isEmpty()) return "Polinomul este null";
        StringBuilder polinomToString = new StringBuilder();

        //primul element (fara + in fata la X)
        int c = polinom.get(0).getCoefficient();
        int p = polinom.get(0).getPower();

        if(c != 0) {
            if (p == 0) {
                polinomToString.append(c);
            } else if (p == 1) {
                if(c == 1){
                    polinomToString.append("X");
                }
                else if (c == -1){
                    polinomToString.append("-X");
                }else{
                    polinomToString.append(c + "X");
                }
            } else{
                if(c == 1){
                    polinomToString.append("X^{" + p + "}");
                }
                else if (c == -1){
                    polinomToString.append("-X^{" + p + "}");
                }else{
                    polinomToString.append(c + "X^{" + p + "}");
                }
            }
        }

        for(int i = 1; i<polinom.size(); i++){
            if(polinom.get(i).getCoefficient() != 0) {
                if (polinom.get(i).getPower() == 0) {
                    if(polinom.get(i).getCoefficient() > 0) {
                        polinomToString.append("+" + polinom.get(i).getCoefficient());
                    }
                    else {
                        polinomToString.append(polinom.get(i).getCoefficient());
                    }
                } else if (polinom.get(i).getPower() == 1) {
                    if(polinom.get(i).getCoefficient() == 1){
                        polinomToString.append("+X");
                    }
                    else if(polinom.get(i).getCoefficient() == -1){
                        polinomToString.append("-X");
                    }
                    else{
                        if(polinom.get(i).getCoefficient() > 0){
                            polinomToString.append("+" + polinom.get(i).getCoefficient() + "X");
                        }
                        else{
                            polinomToString.append(polinom.get(i).getCoefficient()  + "X");
                        }
                    }
                } else{
                    if(polinom.get(i).getCoefficient() == 1){
                        polinomToString.append("+X^{" + polinom.get(i).getPower() + "}");
                    }
                    else if (polinom.get(i).getCoefficient() == -1){
                        polinomToString.append("-X^{" + polinom.get(i).getPower() + "}");
                    }else{
                        if(polinom.get(i).getCoefficient() > 0) {
                            polinomToString.append("+" + polinom.get(i).getCoefficient() + "X^{" + polinom.get(i).getPower() + "}");
                        }
                        else{
                            polinomToString.append(polinom.get(i).getCoefficient() + "X^{" + polinom.get(i).getPower() + "}");
                        }
                    }
                }
            }
        }

        return polinomToString.toString();
    }
}
