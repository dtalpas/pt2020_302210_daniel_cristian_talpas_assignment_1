package PT2020;

import java.util.Collections;

public class Operations {

    public static Polynomial add(Polynomial polinom1, Polynomial polinom2){
        Polynomial result = new Polynomial();

        int i = 0;
        int j = 0;
        int polinom1NoMonoame = polinom1.getMonoameNo();
        int polinom2NoMonoame = polinom2.getMonoameNo();

        while(i < polinom1NoMonoame && j < polinom2NoMonoame){
            int coeff1 = polinom1.getMonomByIndex(i).getCoefficient();
            int coeff2 = polinom2.getMonomByIndex(j).getCoefficient();
            int power1 = polinom1.getMonomByIndex(i).getPower();
            int power2 = polinom2.getMonomByIndex(j).getPower();

            if(power1 == power2){
                result.addMonom(new Monomial(coeff1 + coeff2, power1));
                i++;
                j++;
            }
            else if(power1 < power2){
                result.addMonom(new Monomial(coeff2, power2));
                j++;
            }
            else{
                result.addMonom(new Monomial(coeff1, power1));
                i++;
            }

        }
        if(i < polinom1NoMonoame){
            while(i < polinom1NoMonoame){
                result.addMonom(new Monomial(polinom1.getMonomByIndex(i).getCoefficient(), polinom1.getMonomByIndex(i).getPower()));
                i++;
            }
        }

        if(j < polinom2NoMonoame){
            while (j < polinom2NoMonoame) {
                result.addMonom(new Monomial(polinom2.getMonomByIndex(j).getCoefficient(), polinom2.getMonomByIndex(j).getPower()));
                j++;
            }
        }

        return result;
    }

    public static Polynomial sub(Polynomial polinom1, Polynomial polinom2){
        Polynomial result = new Polynomial();

        int i = 0;
        int j = 0;
        int polinom1NoMonoame = polinom1.getMonoameNo();
        int polinom2NoMonoame = polinom2.getMonoameNo();

        while(i < polinom1NoMonoame && j < polinom2NoMonoame){
            int coeff1 = polinom1.getMonomByIndex(i).getCoefficient();
            int coeff2 = polinom2.getMonomByIndex(j).getCoefficient();
            int power1 = polinom1.getMonomByIndex(i).getPower();
            int power2 = polinom2.getMonomByIndex(j).getPower();

            if(power1 == power2){
                result.addMonom(new Monomial(coeff1 - coeff2, power1));
                i++;
                j++;
            }
            else if(power1 > power2){
                result.addMonom(new Monomial(coeff1, power1));
                i++;
            }
            else{
                result.addMonom(new Monomial(-coeff2, power2));
                j++;
            }
            if(i < polinom1NoMonoame){
                while(i < polinom1NoMonoame){
                    result.addMonom(new Monomial(polinom1.getMonomByIndex(i).getCoefficient(), polinom1.getMonomByIndex(i).getPower()));
                    i++;
                }
            }

            if(j < polinom2NoMonoame){
                while (j < polinom2NoMonoame) {
                    result.addMonom(new Monomial(-polinom2.getMonomByIndex(j).getCoefficient(), polinom2.getMonomByIndex(j).getPower()));
                    j++;
                }
            }

        }

        return result;
    }

    public static Polynomial mul(Polynomial polinom1, Polynomial polinom2) {
        Polynomial result = new Polynomial();
        int produsDegree = polinom1.getDegree() + polinom2.getDegree() + 1;
        int[] resultVector = new int[produsDegree];

        for(int i = 0; i < polinom1.getMonoameNo(); i++){
            int coeff1 = polinom1.getMonomByIndex(i).getCoefficient();
            for(int j = 0; j < polinom2.getMonoameNo(); j++){
                int coeff2 = polinom2.getMonomByIndex(j).getCoefficient();
                resultVector[polinom1.getMonomByIndex(i).getPower() + polinom2.getMonomByIndex(j).getPower()] += coeff1*coeff2;
            }
        }

//        for(int i = 0; i<produsDegree; i++){
//            System.out.println(resultVector[i]);
//        }

        for(int i = 0; i < produsDegree; i++){
            result.addMonom(new Monomial(resultVector[i], i));
        }
        return result;
    }

//    public static Polynomial div(Polynomial polinom1, Polynomial polinom2){
//        Polynomial result = new Polynomial();
//        //algoritm nereusit
//        return result;
//    }

    public static Polynomial deriv(Polynomial givenPolynomial){
        Polynomial result = new Polynomial();

        for(int i = 0; i < givenPolynomial.getMonoameNo(); i++){
            if(givenPolynomial.getMonomByIndex(i).getPower() != 0) {
                int coeff = givenPolynomial.getMonomByIndex(i).getCoefficient();
                int power = givenPolynomial.getMonomByIndex(i).getPower();
                int derivCoeff = coeff * power;
                int derivPower = power - 1;
                result.addMonom(new Monomial(derivCoeff, derivPower));
            }
        }
        return result;
    }

    public static Polynomial integrate(Polynomial givenPolynomial){
        Polynomial result = new Polynomial();

        for(int i = 0; i< givenPolynomial.getMonoameNo(); i++){
            int coeff = givenPolynomial.getMonomByIndex(i).getCoefficient();
            int power = givenPolynomial.getMonomByIndex(i).getPower();
            double derivCoeff = (double)coeff/(power+1);
            double derivPower = power + 1;

            result.addMonom(new Monomial((int)derivCoeff, (int)derivPower));
        }
        return result;
    }
}
